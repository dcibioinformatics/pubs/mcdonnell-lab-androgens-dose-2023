import argparse as ap
import re
import yaml
import functools
import copy
import os
from os import path, makedirs, sys
from multiprocessing import Pool
from hashlib import md5
import subprocess as sp
from pathlib import Path
import tqdm
from time import localtime, strftime

from .containers import resolve_containers, get_param

# recipe contains:
#   list of commands to run in order
#   with inputs listed
#   and outputs listed
#   if inputs list item listed in outputs of previous, it is curried
#
# read in recipe (recipe file)
# read in recipe filename -> actual filename association and parameters (settings file)
# read in sample info (samples file)
# for each sample:
#   resolve files and parameters
#   create working dir
#
##   --- NON-CONTAINER ---
#    run parallel tasks using recipe items in order
#
##     --- CONTAINER ---
#   create any output files
#   set up linking command for pulling linked files into container
#   run parallel tasks using recipe items in order within container
#     with linking command
#
#
#  Priority:
#  1) command
#  2) sample
#  3) settings
#
#

def get_filename(inp, sample, settings, outputs):
  fname = None

  if inp in outputs:
    fname = outputs[inp]

  elif inp in sample:
    fname = Path(sample[inp])

    if not fname.exists() or not fname.is_file():
      raise Exception(f'File not found: {inp}:{fname}')

    fname = fname.resolve()

  elif inp in settings:
    fname = Path(settings[inp])

    if not fname.exists() or not fname.is_file():
      raise Exception(f'File not found: {inp}:{fname}')

    fname = fname.resolve()

  elif settings['print']:

    if inp not in settings['print']['files']:
      settings['print']['files'].append(inp)

      return 'None'

  else:
    raise Exception(f'No file definition for {inp}')

  return fname


# sample has priority, settings as backup
def resolve_files(commands, sample, settings):
  outputs = {}
  for cmd in commands:
    if cmd.get('inputs', False):
      cmd['input_files'] = [(inp, get_filename(inp, sample, settings, outputs), False) for inp in cmd['inputs']]
    else:
      cmd['input_files'] = []

    if cmd.get('outputs', False):
      new_outputs = {item: cmd['outdir'] / item for item in cmd['outputs'] if item not in outputs}
      cmd['output_files'] = [(item, new_outputs[item], True) for item in cmd['outputs']]
      outputs.update(new_outputs)
    else:
      cmd['output_files'] = []

  if not settings['print'] and not settings['test'] > 0:
    for item in outputs.values():
      if not item.exists():
        item.touch()



def create_workdirs(commands, sample, settings):
  if not settings['print'] and 'uid' not in sample:
    raise Exception(f'Missing uid for sample:\n{sample}')
  for cmd in commands:
    if not cmd.get('outdir', False):
      p = Path(settings['workdir'])
      p = p / f"{sample['uid']}" / f"{cmd['name']}"
      cmd['outdir'] = p
    else:
      env = {key: get_param(key, sample, cmd, settings)
             for key in cmd.get('parameters', {})}
      cmd['outdir'] = Path(cmd['outdir'].format(**env))

    if not cmd['outdir'].exists() and not settings['print'] and not settings['test']:
      cmd['outdir'].mkdir(parents=True)


def print_mode(commands, samples, settings):
  settings.update({'print' : {'files': [], 'parameters': []}})

  for idx, cmd in enumerate(commands):
    if "name" not in cmd:
      settings['print']['parameters'].append(f'command "name" for command #{idx+1}')



  for sample in samples:
    create_workdirs(commands, sample, settings)
    resolve_files(commands, sample, settings)
    resolve_containers(commands, sample, settings)

    for key, val in settings['print'].items():
      if val:
        print(f'{sample["uid"]}:')
        print('', f'{key}:', file=sys.stderr)
        for v in val:
          print('', '', f'{v}', file=sys.stderr)


def run(cmd):

  print("Start Time: " + strftime("%Y-%m-%d %H:%M:%S", localtime()))

  outfile = f'{cmd["outdir"]}/{cmd["name"]}.out'
  errfile = f'{cmd["outdir"]}/{cmd["name"]}.err'

  if cmd.get('bash', False):
    cmd['cmd_resolved'] = ['bash', '-c', f'{" ".join(cmd["cmd_resolved"])}']

  if cmd.get('pipe', False) and not (cmd['singularity'] or cmd['docker']):
    with open(errfile, 'w') as err:
      code = sp.call(cmd['cmd_resolved'], stderr=err, shell=False)
  else:
    with open(outfile, 'w') as out, open(errfile, 'w') as err:
      code = sp.call(cmd['cmd_resolved'], stdout=out, stderr=err, shell=False)

  if code != 0:
    raise Exception(f'Error written to {errfile}')

  print("End Time: " + strftime("%Y-%m-%d %H:%M:%S", localtime()))


def run_all(command_groups, threads, slurm, test):
  if slurm:
    for item in command_groups:
      with open(f'{item[0]["name"]}.slurm.sh', 'w') as fh:
        for cmd in item:
          print(' '.join(cmd['cmd_resolved']), file=fh)

  elif test > 0:
    print()
    print('Printing', test, 'samples:')
    print()
    for item in command_groups:
      for cmd in item[0:test]:
        print(' '.join(cmd['cmd_resolved']))
        print()
  else:
    pool = Pool(processes=threads)
    for item in tqdm.tqdm(command_groups):
      pool.map(run, item)


def check_option(cmd, sample, settings):
  if 'option' in cmd:
    p = get_param(cmd['option'], sample, cmd, settings)
    try:
      return eval(p)
    except:
      print('Could not evaluate', file=sys.stderr)
      raise
  else:
    return True


def check_options(commands, sample, settings):
  return [(cmd["name"], cmd) for cmd in commands if check_option(cmd, sample, settings)]


def run_mode(args, commands, samples, settings):
  cmd_groups = []
  names = [cmd['name'] for cmd in commands]
  if settings.get('skip', False):
    try:
      names = names[names.index(settings['skip']):]
    except:
      print(f'cannot skip to {settings["skip"]}, does not exist')
  if settings.get('till', False):
    try:
      names = names[:(names.index(settings['till']) + 1)]
    except:
      print(f'cannot run till {settings["till"]}, does not exist')
  
  commands = [cmd for cmd in commands if cmd['name'] in names]
  for sample in samples:
    cmds = copy.deepcopy(commands)
    create_workdirs(cmds, sample, settings)
    resolve_files(cmds, sample, settings)
    resolve_containers(cmds, sample, settings)
    cmd_groups.append(check_options(cmds, sample, settings))


  groups = []
  for name in names:
    groups.append([x[1] for item in cmd_groups for x in item if x[0] is name])
  run_all(groups, args['python_threads'], args['slurm'], args['test'])


def main(args):
  samples = {}
  settings = {}

  cfile = Path(args['recipe'])

  if not cfile.is_file():
    raise Exception('No recipe file found')

  with open(args['recipe'], 'r') as fh:
    commands = yaml.load(fh, Loader=yaml.FullLoader)

  for cmd in commands:
    cmd['cmd'] = cmd['cmd'].split()

  if args['settings']:
    sp = Path(args['settings'])
    if sp.is_file():
      with sp.open(mode='r') as fh:
        settings = yaml.load(fh, Loader=yaml.FullLoader)
    else:
      raise Exception('No settings file given')
  if args['samples']:
    sap = Path(args['samples'])
    if sap.is_file():
      with sap.open(mode='r') as fh:
        samples = yaml.load(fh, Loader=yaml.FullLoader)
    elif not args['print']:
      raise Exception('No sample file given')
  else:
    samples = [{'uid': 'test'}]

  args['test'] = len(samples) if args['slurm'] else args['test']
  settings.update({key: val for key, val in args.items() if val is not None})
  if not 'workdir' in settings:
    settings['workdir'] = '.'

  if args['print']:
    print_mode(commands, samples, settings)

  else:
    run_mode(args, commands, samples, settings)
