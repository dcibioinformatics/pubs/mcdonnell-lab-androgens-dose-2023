#!/usr/bin/env python

import argparse as ap
from pipeline import main

parser = ap.ArgumentParser()
parser.add_argument('recipe', type=str, help='plan file')
parser.add_argument('-S', '--samples', type=str, help='data file descriptions')
parser.add_argument('-P', '--settings', type=str, help="miscellaneous settings")
parser.add_argument('-n', '--num_threads', dest='python_threads', type=int, default=1, help='number of parallel tasks')
parser.add_argument('-p', '--print', action='store_true', help='print mode, prints all missing items')
parser.add_argument('-t', '--test', type=int, default=0, help='test mode, outputs calls to be made. # of samples to print')
parser.add_argument('-H', '--slurm', action='store_true', dest='slurm', help='print mode formatted for slurm sbatch')
parser.add_argument('-w', '--workdir', dest='workdir', type=str, help='specify output directory, default "."')
parser.add_argument('-s', '--skip-to', dest='skip', type=str, help='skip to step with this name')
parser.add_argument('-e', '--run-till', dest='till', type=str, help='run till step with this name')
if __name__ == '__main__':
    args = vars(parser.parse_args())
    main(args)
  
