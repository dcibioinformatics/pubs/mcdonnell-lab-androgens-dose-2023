
wd=/wd
code_dir=${wd}/Code/ATAC-Seq
ppl=${code_dir}/Pipeline_Base/pipeline.sh
rcp_dir=${code_dir}/Pipeline/Recipes
cfg_dir=${code_dir}/Pipeline/Configs
pre=atac

# -----------------------------------------------
# preprocessing 
${ppl} -n 5 -s FastQC_Init -e FastQC_Init -P ${cfg_dir}/settings.yml -S ${cfg_dir}/samples_all.yml ${rcp_dir}/recipe_all_PE.yml

${ppl} -n 1 -s MultiQC_Init -e MultiQC_Init -P ${cfg_dir}/settings.yml -S ${cfg_dir}/samples_pooled.yml ${rcp_dir}/recipe_pooled.yml

${ppl} -n 5 -s Trimmomatic -e FastQC_Trimmed -P ${cfg_dir}/settings.yml -S ${cfg_dir}/samples_all.yml ${rcp_dir}/recipe_all_PE.yml

${ppl} -n 1 -s MultiQC_Trimmed -e MultiQC_Trimmed -P ${cfg_dir}/settings.yml -S ${cfg_dir}/samples_pooled.yml ${rcp_dir}/recipe_pooled.yml

${ppl} -n 6 -s BWA_ALN_R1 -P ${cfg_dir}/settings.yml -S ${cfg_dir}/samples_all.yml ${rcp_dir}/recipe_all_PE.yml

${ppl} -n 6 -P ${cfg_dir}/settings.yml -S ${cfg_dir}/samples_replicated.yml ${rcp_dir}/recipe_replicated.yml

${ppl} -n 10 -P ${cfg_dir}/settings.yml -S ${cfg_dir}/samples_grouped.yml ${rcp_dir}/recipe_grouped.yml

${ppl} -n 1 -s Jaccard -e IDR -P ${cfg_dir}/settings.yml -S ${cfg_dir}/samples_pooled.yml ${rcp_dir}/recipe_pooled.yml

# analysis
${ppl} -n 4 -s DiffBind -e annotatePeaks -P ${cfg_dir}/settings.yml -S ${cfg_dir}/samples_pooled.yml ${rcp_dir}/recipe_pooled.yml

${ppl} -n 15 -s DiffBind -e DiffBind -P ${cfg_dir}/settings.yml -S ${cfg_dir}/samples_contrasted.yml ${rcp_dir}/recipe_contrasted.yml

${ppl} -n 8 -s bamCompare -e bamCompare -P ${cfg_dir}/settings.yml -S ${cfg_dir}/samples_contrasted.yml ${rcp_dir}/recipe_contrasted.yml

${ppl} -n 8 -s findMotifsGenome -e cpHomerOutputs -P ${cfg_dir}/settings.yml -S ${cfg_dir}/samples_regions1.yml ${rcp_dir}/recipe_regions.yml

${ppl} -n 4 -s findMotifsGenome -e cpHomerOutputs -P ${cfg_dir}/settings.yml -S ${cfg_dir}/samples_regions2.yml ${rcp_dir}/recipe_regions.yml

${ppl} -n 6 -P ${cfg_dir}/settings.yml -S ${cfg_dir}/samples_deeptools.yml ${rcp_dir}/recipe_deeptools.yml
