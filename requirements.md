---
title: "System Requirements and Software Dependencies"
output: 
  html_document:
    keep_md: yes
    toc: false
    number_sections: true
---




## Base Pipeline

- `Singularity` version

```
## 1.2.2
```

- Operating system

```
## Ubuntu 20.04.6 LTS \n \l
```

- `Python` version

```
## Python 3.8.10
```

- `Python` modules

```
## INFO: Successfully output requirements
```


## Container-Based Preprocessing/Analyses Pipeline

- Operating system of the container images

```
## Debian GNU/Linux 11 \n \l
```

- Software version

```
## bedtools v2.29.2
## BWA v0.7.17-r1188
## Deeptools v3.5.1
## FastQC v0.11.9
## Homer v4.11.1
## IDR v2.0.3
## MACS2 v2.2.7.1
## MultiQC v1.10.1
## Phantompeakqualtools (spp) v1.15.2
## Picard MarkDuplicates v2.23.8
## Samtools v1.12
## STAR v2.7.8a
## Trimmomatic v0.39
```



## Container-based `R` Analyses: 

- Operating system of the container image

```
## Ubuntu 20.04.5 LTS \n \l
```

- `R` version

```
## R version 4.2.1 (2022-06-23) -- "Funny-Looking Kid"
## Copyright (C) 2022 The R Foundation for Statistical Computing
## Platform: x86_64-pc-linux-gnu (64-bit)
## 
## R is free software and comes with ABSOLUTELY NO WARRANTY.
## You are welcome to redistribute it under the terms of the
## GNU General Public License versions 2 or 3.
## For more information about these matters see
## https://www.gnu.org/licenses/.
```

- Key `R` packages in alphabetical order

```
##           Package Version
## 1        argparse   2.1.6
## 2        circlize  0.4.15
## 3  ComplexHeatmap  2.12.1
## 4          DESeq2  1.36.0
## 5        DiffBind   3.6.5
## 6              DT    0.26
## 7           fgsea  1.22.0
## 8   GenomicRanges  1.48.0
## 9           ggh4x   0.2.2
## 10        ggrepel   0.9.1
## 11  ggVennDiagram   1.2.2
## 12           glue   1.6.2
## 13      htmltools   0.5.3
## 14     kableExtra   1.3.4
## 15          knitr    1.40
## 16       openxlsx 4.2.5.1
## 17           rsvg   2.3.2
## 18          rvest   1.0.3
## 19      tidyverse   1.3.2
## 20           xlsx   0.6.5
```
