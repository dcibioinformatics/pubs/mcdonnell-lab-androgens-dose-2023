# McDonnell Lab Androgens Dose 2023

This repository contains the scripts used for the manuscript: **Androgen receptor monomers and dimers regulate opposing biological processes in prostate cancer cells**

- **Raw/Processed data:** GSE247593

- **Repository contents**
    - `ATAC-Seq/`: this folder contains pipeline configuration files used for ATAC-Seq data pre-processing and various stages of analyses, including peak calling, and the generation of bigwigs tracks and accessibility heatmaps.     
    - `RNA-Seq/`: this folder contains code used for RNA-Seq data pre-processing (0_dcibioinformatics_rnaseq), QC (1_qc.Rmd), DE and GSEA analyses (2_analyzeQ2dose.Rmd), and printing table and figure outputs (3_print-high-and-low-dose-genes.Rmd, 4_figs-pathways.Rmd, 5_Fig1G.Rmd).

- **software dependencies and operating systems**

Please refer to [requirements.md](./requirements.md) for the complete list of software/packages.

- **License**

This project is licensed under the **GNU General Public License v3.0** - see the [LICENSE](./LICENSE) file for details.
